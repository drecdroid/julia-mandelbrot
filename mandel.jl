function mandelbrot(x0::Float32, x1::Float32, y0::Float32, y1::Float32; res::Integer=200, iters=100) 
    return mandelbrot(
        convert(Float64, x0),
        convert(Float64, x1),
        convert(Float64, y0),
        convert(Float64, y1),
        res=res,
        iters=iters
    )
end

function mandelbrot(x0::Float64, x1::Float64, y0::Float64, y1::Float64; res::Integer=200, iters=100)
    xrange = range(x0, x1, length=res)
    yrange = range(y0, y1, length=res)    
    
    function calciters(cr::Float64, ci::Float64)::Int64
        zr = 0.0
        zi = 0.0

        zrsqr = 0.0
        zisqr = 0.0

        i = 0
        
        while zrsqr + zisqr <= 4.0 && i <= iters
            zi = zr * zi
            zi += zi
            zi += ci
            zr = zrsqr - zisqr + cr
            zrsqr = zr * zr
            zisqr = zi * zi
            i += 1
        end
        
        return i
    end
    
    points = [ calciters(x, y) for x in xrange, y in yrange]::Matrix{Int64}

    return (xrange, yrange, points)
end