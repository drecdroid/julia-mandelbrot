using GLMakie, AbstractPlotting, Observables

include("mandelbrot.jl")

function main()
    f = Figure(resolution=(1200, 1200))    

    iters = 100
    res = 600
    
    ox = mandelbrot(-2.0, 1.0, -1.5, 1.5, res=res, iters=iters)

    xs = Node(ox[1])
    ys = Node(ox[2])
    zs = Node(ox[3])


    ax = f[1,1] = Axis(
        f,
        xminorticksvisible=true,
        yminorticksvisible=true,
        aspect=DataAspect(),
        xticks=LinearTicks(10),
        yticks=LinearTicks(10)
    )

    function update_plot()
        limits = ax.targetlimits[]
        (x0, y0) = convert(Vec{2,Float64}, limits.origin)
        (w, h) = convert(Vec{2,Float64}, limits.widths)

        ox = mandelbrot(x0, x0 + w, y0, y0 + h, res=res, iters=iters)

        xs[] = ox[1]
        ys[] = ox[2]
        zs[] = ox[3]
    end
           
    heatmap!(f[1,1], xs, ys, zs, colormap=Reverse(:darktest), interpolate=true)
    
    iters_slider = Slider(f[2, 1], range=100:100:1000, startvalue=100)
    res_slider = Slider(f[3, 1], range=600:100:2400, startvalue=600)

    on(iters_slider.value) do val
        iters = val
        update_plot()
    end

    on(res_slider.value) do val
        res = val
        update_plot()
    end

    on(throttle(0.25, ax.targetlimits)) do lims
        limits = ax.targetlimits[]
        (x0, y0) = convert(Vec{2,Float64}, limits.origin)
        (w, h) = convert(Vec{2,Float64}, limits.widths)

        xs[] = 
        update_plot()
    end
    
    return f
end