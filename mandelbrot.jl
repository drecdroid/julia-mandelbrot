const Float64StepRangeLen = StepRangeLen{Float64,Base.TwicePrecision{Float64},Base.TwicePrecision{Float64}}

function calciters(cr::Float64, ci::Float64, iters)::Int64
    zr = 0.0
    zi = 0.0

    zrsqr = 0.0
    zisqr = 0.0

    i = 0
    
    while zrsqr + zisqr <= 4.0 && i <= iters
        zi = zr * zi
        zi += zi
        zi += ci
        zr = zrsqr - zisqr + cr
        zrsqr = zr * zr
        zisqr = zi * zi
        i += 1
    end
    
    return i
end

function mandelbrot(xrange::Float64StepRangeLen, yrange::Float64StepRangeLen; iters=100)::Matrix{Int64}        
    return [ calciters(x, y, iters) for x in xrange, y in yrange ]::Matrix{Int64}
end